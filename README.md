# Facial Keypoint Detection

Facial Keypoint Detection is an important problem in computer vision, It can be used for face verification, mask detection, emotion and sentiment detection, age estimation, ethnicity detection, gender detection and many other problems.

This is an implementation of two papers [(1)](#ref1) [(2)](#ref2) to compete in [Kaggle Facial Keypoint detection challenge](https://www.kaggle.com/c/facial-keypoints-detection), I am using the challenge data to evaluate and train the algorithms, I am also submitting to the challenge to better evaluate the algorithms. 

## Install requirements:

To install the requirements

```
pip install -r requirements.txt
```

## Training:

To Train

```
python main.py --config conf/MLResNet.yaml --train
```

## Making Submission:

To make the submission File

```
python main.py --config conf/MLResNet.yaml
```

Don't forget to configure `test_checkpoint` parameter in the configuration file.

## Demo:

<img src="img/view-1-predicted.jpg"
     alt="Target 1"
     style="float: center; margin: 5px; display: inline; padding-left: 45%; width: 200px; height: 200px" />
<img src="img/view-1-target.jpg"
     alt="Target 1"
     style="float: center;  margin: 5px; display: inline; width: 200px; height: 200px" />
<br>
<img src="img/view-2-predicted.jpg"
     alt="Target 1"
     style="float: center; margin: 5px; display: inline; padding-left: 45%; width: 200px; height: 200px" />
<img src="img/view-2-target.jpg"
     alt="Target 1"
     style="float: center;  margin: 5px; display: inline; width: 200px; height: 200px" />

On the left (with red markers) are the predicted keypoints by MLResNet-v1.1 (Best Model) and on the right (with green markers) the ground truth labels provided by the dataset (images take from the validation set). 
## Current Project State:

- **[30-10-2020]** After Training Simple CNN, LeNet5 and Simple VGG, but still MlResNet has the best score,
maybe they need some hyperparameter tunning or just need batch Normalization layers, or likely both :) :) . 

- **[26-02-2020]** I have Trained MLResNet for 11000 Epoch and got the best Epoch at 2037 ([MLResNet-v1.1](conf/MLResNet-v1.1.yaml)) using [this checkpoint](https://drive.google.com/file/d/1ygFm4f0AqTcpQZCO4V6e4JNIKjiGH-lF/view?usp=sharing) and I got public score of 5.44676,
Which should make me at position 156 (If the leaderboard was open).

- **[22-02-2020]** I have made trained my first model (MLResNet-v1.0) and I have made my first submission and had a score of 7.59132, 
which should make me in 157 on public leaderbord (But the competition is closed - So I cannot be on the Leader Board - Sorry).

## References:

1. <a name="ref1">[A Deep Residual convolutional neural network for facial keypoint detection with missing label](https://www.sciencedirect.com/science/article/pii/S0165168417303924)</a>
2. <a name="ref2">[Facial Keypoint Detection with Convolutional Neural Networks](https://ieeexplore.ieee.org/abstract/document/9065279)</a>
