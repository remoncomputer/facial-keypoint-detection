from typing import List

import os

import torch
from torch.utils.data import DataLoader, random_split, TensorDataset
# import torchvision.transforms.transforms

import pytorch_lightning as pl
from pytorch_lightning import seed_everything

import pandas as pd
import numpy as np
import imageio
import cv2 as cv

# from tqdm import tqdm


def debug_image(tensor_img, key_points=None, mask=None,
                marker_type=cv.MARKER_CROSS, marker_color=(255, 0, 0),
                img_path='~/Downloads/debug.jpg'):
    numpy_img_transposed = tensor_img.detach().cpu().numpy().astype(np.uint8)
    numpy_img_grayscale = np.transpose(numpy_img_transposed, axes=(1, 2, 0))
    if numpy_img_grayscale.shape[-1] == 1:
        numpy_img_grayscale = np.squeeze(numpy_img_grayscale)
        numpy_img = np.empty(
            (*numpy_img_grayscale.shape[:2], 3), dtype=np.uint8)
        numpy_img[:, :, 0] = numpy_img_grayscale
        numpy_img[:, :, 1] = numpy_img_grayscale
        numpy_img[:, :, 2] = numpy_img_grayscale
    else:
        numpy_img[:, :, 2] = numpy_img_grayscale
    if key_points is not None:
        key_points = torch.round(key_points).long()
        if mask is not None:
            key_points *= mask
        key_points = key_points.reshape((-1, 2))
        for (x, y) in key_points:
            if x > 0 and y > 0:
                cv.drawMarker(
                    numpy_img, (x, y), marker_color, marker_type, 5)
    imageio.imsave(img_path, numpy_img)


class CustomTensorDataset(torch.utils.data.Dataset):
    r'''Customized Tensor Dataset to support the usage of transforms'''
    def __init__(self, tensors: List[torch.Tensor], transform=None):
        r'''Args:
                tensors (List[torch.Tensor]): The Tensors to be used in the
                    dataset.
                transform (torchvision Transform): The Tansform to be applied
         '''
        assert all(tensors[0].size(0) == ten.size(0) for ten in tensors)
        self.tensors = tensors
        self.transform = transform

    def __len__(self):
        r'''Returns the Length of the dataset'''
        return self.tensors[0].size(0)

    def __getitem__(self, idx):
        r'''Return an item and optionally applies a Transform'''
        item = (tensor[idx] for tensor in self.tensors)
        if self.transform:
            item = self.transform(*item)
        return item

# class SubsetDatasetDecoratorWithTransform(Subset):
#     r'''Custom class to override subset but adds a transformations to it'''
#     def __init__(self, parent_instance: Subset, transform=None):
#         r'''Args:
#                 parent_instance (Subset): the parent instance to wrap


class CollateFunctionWithTransform:
    r'''This works like the Dataset collate function but it also applies a
    transform'''
    def __init__(self, transform=None):
        self.transform = transform

    def __call__(self, batch):
        batched_tensors = [torch.stack(ten) for ten in zip(*batch)]
        if self.transform:
            batched_tensors = self.transform(batched_tensors)
        return batched_tensors


class FacialKeyPointDataset(pl.LightningDataModule):
    def __init__(self, dataset_root, batch_size=64,
                 training_data_percentage=0.9, train_file_name='training.csv',
                 test_file_name='test.csv', img_width=96, img_height=96,
                 n_channels=1, train_transform=None, val_transform=None,
                 test_tranform=None, n_workers=8):
        super(FacialKeyPointDataset, self).__init__(
            train_transforms=train_transform,
            val_transforms=val_transform,
            test_transforms=test_tranform,
            dims=(n_channels, img_height, img_width)
        )
        self.dataset_root = dataset_root
        self.batch_size = batch_size
        self.training_data_percentage = training_data_percentage
        self.train_file_name = train_file_name
        self.test_file_name = test_file_name
        self.n_channels = n_channels
        if not (0 < training_data_percentage <= 1):
            raise ValueError(
                "training_data_percentage should be "
                + "0 < training_data_percentage <= 1 but it is "
                + f"{training_data_percentage}")
        self.img_width = img_width
        self.img_height = img_height
        self.n_workers = n_workers
        # self.dims = (n_channels, img_height, img_width)
        self.column_list = [
            'left_eye_center_x',
            'left_eye_center_y',
            'right_eye_center_x',
            'right_eye_center_y',
            'left_eye_inner_corner_x',
            'left_eye_inner_corner_y',
            'left_eye_outer_corner_x',
            'left_eye_outer_corner_y',
            'right_eye_inner_corner_x',
            'right_eye_inner_corner_y',
            'right_eye_outer_corner_x',
            'right_eye_outer_corner_y',
            'left_eyebrow_inner_end_x',
            'left_eyebrow_inner_end_y',
            'left_eyebrow_outer_end_x',
            'left_eyebrow_outer_end_y',
            'right_eyebrow_inner_end_x',
            'right_eyebrow_inner_end_y',
            'right_eyebrow_outer_end_x',
            'right_eyebrow_outer_end_y',
            'nose_tip_x',
            'nose_tip_y',
            'mouth_left_corner_x',
            'mouth_left_corner_y',
            'mouth_right_corner_x',
            'mouth_right_corner_y',
            'mouth_center_top_lip_x',
            'mouth_center_top_lip_y',
            'mouth_center_bottom_lip_x',
            'mouth_center_bottom_lip_y'
        ]
        self.feature_to_idx = self.compute_column_dict(self.column_list)

    def compute_column_dict(self, column_list):
        name_to_idx_dict = {
            col_name: idx for idx, col_name in enumerate(column_list)}
        return name_to_idx_dict

    def prepare_data(self):
        train_file_path = os.path.join(self.dataset_root, self.train_file_name)
        if not os.path.exists(train_file_path):
            raise ValueError(f"Train file path: {train_file_path} not found")
        test_file_path = os.path.join(self.dataset_root, self.test_file_name)
        if not os.path.exists(test_file_path):
            raise ValueError(f"Test file path: {test_file_path} not found")

    def setup(self, stage=None):
        if stage == 'fit' or stage is None:
            train_file_path = os.path.join(
                self.dataset_root, self.train_file_name)
            print('Loading Training dataset')
            train_df = pd.read_csv(train_file_path, header=0, index_col=False)
            n_images = len(train_df)
            train_coordinates_df = train_df[self.column_list]
            train_coordinates = torch.from_numpy(train_coordinates_df.to_numpy(
                na_value=-1, dtype=np.float32))
            train_coordinates_mask = \
                (train_coordinates > -1)  # mask for missing values
            train_images_df = train_df['Image'].apply(
                lambda img_str: [int(el) for el in img_str.split()])
            train_images_tensor_flat = torch.tensor(
                train_images_df, dtype=torch.float32)
            train_images = train_images_tensor_flat.view(
                -1, self.n_channels, self.img_height, self.img_width)
            full_train_data = TensorDataset(train_images, train_coordinates,
                                            train_coordinates_mask)
            n_train = int(np.floor(n_images * self.training_data_percentage))
            n_val = n_images - n_train
            self.train_dataset, self.val_dataset = \
                random_split(full_train_data, [n_train, n_val])
        elif stage == 'test':
            test_file_path = os.path.join(
                self.dataset_root, self.test_file_name)
            print('Loading Test dataset')
            test_df = pd.read_csv(test_file_path, header=0, index_col=False)
            # n_images = len(test_df)
            test_image_ids = torch.from_numpy(
                test_df['ImageId'].to_numpy(dtype=np.int32))
            test_images_df = test_df['Image'].apply(
                lambda img_str: [int(el) for el in img_str.split()])
            test_images_tensor_flat = torch.tensor(
                test_images_df, dtype=torch.float32)
            test_images = test_images_tensor_flat.view(
                -1, self.n_channels, self.img_height, self.img_width)
            self.test_dataset = TensorDataset(test_images, test_image_ids)
        else:
            raise ValueError(f"Unknown stage: {stage}")

    def train_dataloader(self):
        return DataLoader(
            self.train_dataset, batch_size=self.batch_size,
            shuffle=True, num_workers=self.n_workers,
            pin_memory=True,
            collate_fn=CollateFunctionWithTransform(self.train_transforms))

    def val_dataloader(self):
        return DataLoader(
            self.val_dataset,
            batch_size=self.batch_size,
            shuffle=False,
            num_workers=self.n_workers,
            pin_memory=True,
            collate_fn=CollateFunctionWithTransform(self.val_transforms))

    def test_dataloader(self):
        return DataLoader(
            self.test_dataset,
            batch_size=self.batch_size,
            shuffle=False,
            num_workers=1,  # No parallisim
            pin_memory=True,
            collate_fn=CollateFunctionWithTransform(self.test_transforms))


# Unit testing the dataset
if __name__ == "__main__":
    seed_everything(0)
    dataset_ins = FacialKeyPointDataset(
        dataset_root='data/facial-keypoint-dataset')
    dataset_ins.prepare_data()
    dataset_ins.setup()
    dataset_ins.setup('test')
    train_loader = dataset_ins.train_dataloader()
    val_dataloader = dataset_ins.val_dataloader()
    test_dataloader = dataset_ins.test_dataloader()
    one_img, key_points = dataset_ins.train_dataset[0]
    debug_image(one_img, key_points=key_points)
    print('Here')
