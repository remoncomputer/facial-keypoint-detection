import torch
import torch.optim
from torch import nn

import pytorch_lightning as pl

import pandas as pd

from loss import MaskedMeanSquaredError


class MLResNetResidualBlock(nn.Module):
    def __init__(self, input_channels: int, output_channels: int,
                 kernel_size=3):
        '''
        Residual block for MLResNet,
        I am not sure if this is the correct implementation of the paper or not
        '''

        super(MLResNetResidualBlock, self).__init__()
        padding = (kernel_size - 1) // 2

        # works as a channel count converter and adaptive pool layer
        self.first_conv = nn.Conv2d(
            input_channels, output_channels, kernel_size=kernel_size,
            padding=padding, stride=2)

        self.sequential = nn.Sequential(
            nn.Conv2d(output_channels, output_channels,
                      kernel_size=kernel_size, padding=padding),
            nn.BatchNorm2d(output_channels),
            nn.ReLU(),
            nn.Conv2d(output_channels, output_channels,
                      kernel_size=kernel_size, padding=padding),
            nn.BatchNorm2d(output_channels),
            nn.ReLU()
        )

    def forward(self, X: torch.Tensor):
        X = self.first_conv(X)
        y = self.sequential(X)
        y = y + X
        return y


class MLResNet(nn.Module):
    def __init__(self, input_size: tuple, first_conv_kernel_size: int,
                 first_conv_channels: int, residual_features: list,
                 fully_connected_neurons: int, output_size: int,
                 dropout_p: float):
        '''
        Backend model, this model is suggesed by
        [A Deep Residual convolutional neural network for facial \
            keypoint detection with missing label]
        (https://www.sciencedirect.com/science/article/pii/S0165168417303924)
        '''

        super(MLResNet, self).__init__()
        input_channels, input_height, input_width = input_size
        padding = (first_conv_kernel_size - 1) // 2
        conv1 = nn.Conv2d(input_channels, first_conv_channels,
                          kernel_size=first_conv_kernel_size, padding=padding)
        relu1 = nn.ReLU()
        maxpool1 = nn.MaxPool2d(kernel_size=2)
        modules_list = nn.ModuleList([conv1, relu1, maxpool1])
        residual_features = [first_conv_channels] + residual_features
        for idx in range(len(residual_features) - 1):
            res_block = MLResNetResidualBlock(
                residual_features[idx], residual_features[idx + 1])
            modules_list.append(res_block)
            modules_list.append(nn.Dropout(p=dropout_p))
        # Adding a flattening module
        modules_list.append(nn.Flatten())
        input_width_after_res_blocks = \
            input_width // (2 ** len(residual_features))
        input_height_after_res_blocks = \
            input_height // (2 ** len(residual_features))
        final_res_channels = residual_features[-1]
        input_to_fully_connected = \
            input_height_after_res_blocks * input_width_after_res_blocks * \
            final_res_channels
        pre_final_full_connected = \
            nn.Linear(input_to_fully_connected, fully_connected_neurons)
        modules_list.append(pre_final_full_connected)
        modules_list.append(nn.ReLU())
        final_classifer_layer = nn.Linear(fully_connected_neurons, output_size)
        modules_list.append(final_classifer_layer)
        self.sequential = nn.Sequential(*modules_list)

    def forward(self, X: torch.Tensor):
        return self.sequential(X)


class SimpleCNN(nn.Module):
    '''
    Simple CNN Class can be used to implement the Simple CNN Network,
        LeNet5 Network and Simple VGG style Network, found in
        [Facial Keypoint Detection with Convolutional Neural Networks]\
            (https://ieeexplore.ieee.org/abstract/document/9065279)
    '''

    def __init__(self, VGG_style: bool, input_dim: tuple, cnn_features: list,
                 cnn_kernel_size: int, fully_connected_neurons: list,
                 dropout_p: float, output_size: int):
        '''
        Parameters:
        ------------
        VGG_style: boolean
            if False then there will be one conv in the
            block, if True there will be two conv

        input_dim: typle
            indicating the dim of input -> (C,H,W) pytorch style images.

        cnn_features: list of int
            indicating the features of the conv layers.

        cnn_kernel_size: int
            The Kernel Size of the CNN

        fully_connected_neurons: list of int
            indicating the fully connected neurons dim.

        dropout_p: float
            the dropout probability

        output_size: int
            the number of output units
        '''
        super(SimpleCNN, self).__init__()
        # The list of module that I will use
        module_list = nn.ModuleList()
        input_channels, input_height, input_width = input_dim
        cnn_features = [input_channels] + cnn_features
        current_channel_width = input_width
        current_channel_height = input_height
        # Adding the CNN blocks (Conv + Relu + Max Pool + dropout)
        for idx in range(len(cnn_features) - 1):
            module_list.append(
                nn.Conv2d(cnn_features[idx],
                          cnn_features[idx + 1], kernel_size=cnn_kernel_size)
                )
            current_channel_height -= cnn_kernel_size - 1
            current_channel_width -= cnn_kernel_size - 1
            module_list.append(nn.ReLU())
            if VGG_style:
                module_list.append(
                    nn.Conv2d(
                        cnn_features[idx+1], cnn_features[idx + 1],
                        kernel_size=cnn_kernel_size)
                    )
                current_channel_height -= cnn_kernel_size - 1
                current_channel_width -= cnn_kernel_size - 1
                module_list.append(nn.ReLU())
            module_list.append(nn.MaxPool2d(kernel_size=2))
            current_channel_height //= 2
            current_channel_width //= 2
            module_list.append(nn.Dropout(dropout_p))
        # Adding a flattening module
        module_list.append(nn.Flatten())
        # calculating input to fully connected layer
        input_size_to_fully_connected = \
            current_channel_height * current_channel_width * cnn_features[-1]
        # Adding the fully connected layers (Linear + Relu)
        fully_connected_neurons = [input_size_to_fully_connected] \
            + fully_connected_neurons
        for idx in range(len(fully_connected_neurons) - 1):
            module_list.append(
                nn.Linear(fully_connected_neurons[idx],
                          fully_connected_neurons[idx + 1]))
            module_list.append(nn.ReLU())
        # Adding final layer output
        module_list.append(nn.Linear(fully_connected_neurons[-1], output_size))
        # Making the sequential that will hold the model
        self.seq = nn.Sequential(*module_list)

    def forward(self, X):
        return self.seq(X)


def get_optimizer(
        optimizer_config: dict, model_parameters: torch.nn.ParameterList):
    '''
    Factory menthod for the optimizer, it gets the optimizer from pytorch
    and sets it's parameters
    '''

    optimizer_class_name = optimizer_config.pop('Name')
    optimizer_class = getattr(torch.optim, optimizer_class_name)
    optimizer_instance = optimizer_class(model_parameters, **optimizer_config)
    return optimizer_instance


def get_backend_model(model_config):
    '''
    Factory Method to return backend models
    '''
    model_name = model_config.pop('Name')

    model_name_vs_class = {
        'MLResNet': MLResNet,
        'SimpleCNN': SimpleCNN
    }

    if model_name not in model_name_vs_class:
        raise ValueError(
                f"Model {model_name} is not implemented yet make sure you "
                + "implement it or type the name correctly")
    model_class = model_name_vs_class[model_name]
    return model_class(**model_config)


class PLModel(pl.LightningModule):
    '''
    This is the Parent Pytorch Lightining Model,
    that holds one of the other backend models
    (one of the above models)
    '''

    def __init__(self, backend_model_config: dict, optimizer_config: dict,
                 submission_file_path: str, output_submission_file_path: str,
                 feature_name_to_idx: dict):
        super().__init__()
        self.backend_model = get_backend_model(backend_model_config)
        self.optimizer_config = optimizer_config
        self.feature_name_to_idx = feature_name_to_idx
        self.loss_function = MaskedMeanSquaredError
        self.submission_file_path = submission_file_path
        self.feature_name_to_idx = feature_name_to_idx
        self.output_submission_file_path = output_submission_file_path
        self.testDF = None

    def forward(self, X: torch.Tensor):
        return self.backend_model(X)

    def configure_optimizers(self):
        return get_optimizer(self.optimizer_config,
                             self.backend_model.parameters())

    def training_step(self, batch: tuple, batch_idx: int):
        images, target_coordinates, mask = batch
        predicted_coordinates = self.backend_model(images)
        meanSquaredErrorLossValue = self.loss_function(
            predicted_coordinates, target_coordinates, mask)
        self.log('Train Batch RMSE', torch.sqrt(meanSquaredErrorLossValue))
        return {'loss': meanSquaredErrorLossValue}

    def training_epoch_end(self, outputs: list):
        avg_train_loss = torch.tensor([val['loss'] for val in outputs]).mean()
        self.log('Train Epoch RMSE', torch.sqrt(avg_train_loss))
        # return {'avg_train_loss': avg_train_loss}

    def validation_step(self, batch: torch.Tensor, batch_idx: int):
        images, target_coordinates, mask = batch
        predicted_coordinates = self.backend_model(images)
        meanSquaredErrorLossValue = self.loss_function(
            predicted_coordinates, target_coordinates, mask)
        self.log('Val Batch RMSE', torch.sqrt(meanSquaredErrorLossValue))
        return {'val_loss': meanSquaredErrorLossValue}

    def validation_epoch_end(self, outputs: list):
        avg_val_loss = torch.sqrt(torch.tensor(
            [val['val_loss'] for val in outputs]).mean())
        self.log('Val Epoch RMSE', avg_val_loss)
        return {'avg_val_loss': avg_val_loss}

    def test_step(self, batch: torch.Tensor, batch_idx: int):
        if self.testDF is None:
            self.testDF = pd.read_csv(self.submission_file_path, header=0,
                                      index_col=False)
            self.submission_file_length = len(self.testDF)
            self.last_submission_row_idx = 0
        images, batch_image_ids = batch
        img_dim = images.shape[-1]
        batch_image_ids = [el.item() for el in batch_image_ids]
        predicted_coordinates = self.backend_model(images)
        while self.last_submission_row_idx < self.submission_file_length:
            current_row = self.testDF.iloc[self.last_submission_row_idx]
            image_id = current_row['ImageId']
            feature_name = current_row['FeatureName']
            if image_id not in batch_image_ids:
                return
            image_in_batch_idx = batch_image_ids.index(image_id)
            feature_idx = self.feature_name_to_idx[feature_name]
            predicted_feature_location = \
                predicted_coordinates[image_in_batch_idx, feature_idx].item()
            predicted_feature_location = max(predicted_feature_location, 0)
            predicted_feature_location = min(predicted_feature_location,
                                             img_dim)
            self.testDF['Location']\
                .iloc[self.last_submission_row_idx] = \
                predicted_feature_location
            self.last_submission_row_idx += 1
        return

    def test_epoch_end(self, outputs: list):
        new_df = self.testDF.drop(columns=['ImageId', 'FeatureName'])
        new_df.to_csv(self.output_submission_file_path, index=False)
