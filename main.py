from argparse import ArgumentParser

import torchvision

from pytorch_lightning import Trainer, seed_everything
# from pytorch_lightning.tuner.tuning import Tuner
from pytorch_lightning.callbacks import ModelCheckpoint
from pytorch_lightning.loggers.tensorboard import TensorBoardLogger

import yaml
from yaml import Loader

from dataset import FacialKeyPointDataset
from model import PLModel
from transform import RandomAffineBatchTranform, RandomBatchHorizonalFlipping

if __name__ == "__main__":
    # Parsing the args to the script
    parser = ArgumentParser()
    parser.add_argument("--config", type=str, required=True,
                        default=None, help="The config file for training")
    parser.add_argument("--train", action="store_true",
                        help="Use this flag to start training")
    args = parser.parse_args()
    config_file_path = args.config
    train_mode = args.train

    # Opening the configuration file
    with open(config_file_path, mode='r') as f:
        config = yaml.load(f, Loader=Loader)

    if train_mode:
        print(f"Script will start training using {config_file_path}")
    else:
        print(f"Script will make submission file using {config_file_path}")
        print(f"Script will be using checkpoint: {config['test_checkpoint']}")

    # force everything to be deterministic
    random_seed = config.pop('random_seed')
    seed_everything(random_seed)

    # Configuring the Transformations
    train_transform = torchvision.transforms.Compose([
        RandomAffineBatchTranform(
            **config['transforms_config']['RandomAffineBatchTranform']),
        RandomBatchHorizonalFlipping(
            **config['transforms_config']['RandomBatchHorizonalFlipping'])
    ])
    val_transform = None
    test_transform = None

    # Reading and loading the dataset
    dataset_config = config.pop('dataset_config')
    # need to load the transforms and pass them to dataset
    dataset = FacialKeyPointDataset(
        **dataset_config,
        train_transform=train_transform,
        val_transform=val_transform,
        test_tranform=test_transform
        )

    # Reading and initializing the model
    model_config = config.pop('model_config')
    if train_mode:
        model = PLModel(
            **model_config,
            feature_name_to_idx=dataset.feature_to_idx)
    else:
        test_checkpoint_path = config.pop('test_checkpoint')
        model = PLModel.load_from_checkpoint(
            test_checkpoint_path,
            **model_config,
            feature_name_to_idx=dataset.feature_to_idx
            )
        # to avoid overiding the loaded checkpoint
        if 'resume_from_checkpoint' in config['trainer_config']:
            del config['trainer_config']['resume_from_checkpoint']

    # Reading and intializing Callback configurations
    callbacks_config = config.pop('callbacks')
    checkpoint_callback = ModelCheckpoint(
        **callbacks_config['ModelCheckpoint']
        )

    callbacks = []

    # Reading and intializing Logger configurations
    loggers_config = config.pop('loggers')
    loggers = [
        TensorBoardLogger(**loggers_config['TensorBoardLogger']),
    ]

    # Reading and intilaizing the Trainer
    trainer_config = config.pop('trainer_config')
    if 'resume_from_checkpoint' in trainer_config and \
            trainer_config['resume_from_checkpoint'] is not None:
        print('Training will resume from: '
              + f'{trainer_config["resume_from_checkpoint"]}')

    trainer = Trainer(
        **trainer_config,
        callbacks=callbacks,
        logger=loggers,
        checkpoint_callback=checkpoint_callback,
        auto_scale_batch_size=(dataset_config['batch_size'] is None)
    )

    # optimizing batch size if batch_size is none
    if dataset_config['batch_size'] is None:
        print("Batch Size is None attempting to tune batch size")
        # tuner = Tuner(trainer)
        # optimal_batch_size = tuner.scale_batch_size(
        #     model, mode='power',
        #     batch_arg_name='batch_size',
        #     datamodule=dataset)
        trainer.tune(model, datamodule=dataset)
        # print(f"Found best batch size to be: {optimal_batch_size}")
        # dataset.batch_size = optimal_batch_size

    # Finally Training or testing
    if train_mode:
        print("Training will start now")
        trainer.fit(model=model, datamodule=dataset)
        print("Training Finished!!")
    else:
        print("Submission file generation will start now")
        print("Submission file will be written to: "
              + f"{model_config['output_submission_file_path']}")
        trainer.test(model=model, datamodule=dataset)
        print("Submission file generation finished successfully!!")
