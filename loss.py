import torch

from torch import Tensor


def MaskedMeanSquaredError(predictions: Tensor, targets: Tensor, mask: Tensor):
    '''
    Masked mean squared error, with a smarter trick
    from [A Deep Residual convolutional neural network for \
        facial keypoint detection with missing label]
    (https://www.sciencedirect.com/science/article/pii/S0165168417303924)
    '''

    raw_losses = torch.nn.functional.\
        mse_loss(predictions, targets, reduction='none')
    masked_losses = raw_losses * mask
    masked_losses_sum = masked_losses.sum()
    n_valid_metrics = mask.sum().float()
    final_loss_value = masked_losses_sum / n_valid_metrics
    return final_loss_value


if __name__ == "__main__":
    '''
    Unit testing the losses
    '''

    predictions = torch.tensor([[2, 3], [4, 5]], dtype=torch.float32)
    targets = torch.tensor([[1, -1], [3, -1]], dtype=torch.float32)
    mask = (targets > -1)
    computed_loss = MaskedMeanSquaredError(predictions, targets, mask)
    assert computed_loss.item() == ((2-1)**2 + (4-3)**2)/2
    print("Tests passed successfully")
