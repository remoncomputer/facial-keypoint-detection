from typing import Optional, Tuple

import torch

from kornia.geometry.transform import get_affine_matrix2d, warp_affine, hflip


def get_random_range_tensor_for_batch(batch_size: int,
                                      range_values: Tuple[any, any],
                                      dtype=torch.float32):
    '''
    This function gets a tensor of certain batch size
     to that has a certain range
    and has a certain tensor type

    Parameters:
    -----------

    batch_size: int,
        The size of batch to return tensor random values tensor to

    range_value: tuple of (min_val, max_val),
        The min and max values to return values between them

    dtype: the final type of the returned tensor

    '''
    random_values = torch.rand((batch_size, 1), dtype=torch.float32)
    min_val, max_val = range_values
    random_values_in_range = min_val + random_values * (max_val - min_val)
    return random_values_in_range.type(dtype)


def dummy(imgs, coord, mask):
    '''
    Dummy Transform to test if the transform is called or not
    '''
    print("Dummy Transform called")
    print(f"imgs Size: {imgs.shape}, coord Size: {coord.shape}, \
        mask Size: {mask.shape}")


class RandomAffineBatchTranform:
    '''
    Applies Random affine transform with rotation angle, scale, translation
    and shear
    '''
    def __init__(self, translation_ratio_range_x: Tuple[float, float],
                 translation_ratio_range_y: Tuple[float, float],
                 scale_ratio_range: Tuple[float, float],
                 angle_range_in_degrees: Tuple[float, float],
                 shear_factor_x_range: Optional[torch.Tensor] = None,
                 shear_factor_y_range: Optional[torch.Tensor] = None):
        '''
        Parameters:
        -----------

        translation_ratio_range_x: tuple of float (ex: (-0.25, 0.25))
            The range of ratios of length and height to use for random
            translation in x-direction

        translation_ratio_range_y: tuple of float (ex: (-0.25, 0.25))
            The range of ratios of length and height to use for random
            translation in y-direction

        scale_ratio_range: tuple of float (ex: (0.5, 1.5))
            The range of ratios to choose for random scaling

        angle_range_in_degrees: tuple of float(ex: (-10, 10))
            The range of angles to use for random rotation

        shear_factor_x_range: tuple of float
            The range of factors to use for random shear in X-Direction

        shear_factor_y_range: tuple of float
            The range of factors to use for random shear in Y-Direction
        '''
        self.translation_ratio_range_x = translation_ratio_range_x
        self.translation_ratio_range_y = translation_ratio_range_y
        self.scale_ratio_range = scale_ratio_range
        self.angle_range_in_degrees = angle_range_in_degrees
        self.shear_factor_x_range = shear_factor_x_range
        self.shear_factor_y_range = shear_factor_y_range

    def generate_random_translations(self, batch_size: int, img_height: int,
                                     img_width: int):
        '''
        Generate the Random Translations for the batch
        '''
        translation_x_range = (
            val * img_width for val in self.translation_ratio_range_x)
        random_x_translations = get_random_range_tensor_for_batch(
            batch_size,
            translation_x_range)
        translation_y_range = (
            val * img_height for val in self.translation_ratio_range_y)
        random_y_translations = get_random_range_tensor_for_batch(
            batch_size, translation_y_range)
        translation_batch = torch.cat(
            [random_x_translations, random_y_translations], dim=1)
        return translation_batch

    def generate_scale_factors(self, batch_size: int):
        '''
        Generation of the scale factors for scalling the image
        '''
        scale_factors = get_random_range_tensor_for_batch(
            batch_size, self.scale_ratio_range)
        batch_scaling_factors = torch.cat(
            [scale_factors, scale_factors], dim=1)
        return batch_scaling_factors

    def generate_random_angles(self, batch_size: int):
        '''
        Generates random angles for the batch
        '''
        return torch.squeeze(get_random_range_tensor_for_batch(
            batch_size,
            self.angle_range_in_degrees))

    def generate_shear_factors(self, batch_size: int,
                               shear_range: Tuple[float, float]):
        '''
        Returns:
            The shear factors for the batch,
                can be used with shear_x or shear_y
        '''
        if shear_range is None:
            return None
        return get_random_range_tensor_for_batch(batch_size, shear_range)

    def generate_image_center_for_batch(self, batch_size: int, img_height: int,
                                        img_width: int):
        '''
        Return the center at which we will perform tha affine about,
        Which is always the center of the image.
        '''
        center_x = img_width / 2
        center_y = img_height / 2
        center_batch = torch.tensor([center_x, center_y],
                                    dtype=torch.float32).repeat(
                                        (batch_size, 1))
        return center_batch

    def transform_coordinates(self, batch_tranformation_matrix: torch.Tensor,
                              coordinates: torch.Tensor):
        r'''This function will return the tranformed coordinates.

        Args:
            batch_tranformation_matrix (torch.Tensor): The Affine
                Transformation Matrix
            coordinates (torch.Tensor): The x,y coordinates for tha facial
                keypoints
        Returns:
            torch.Tensor: The transformed coordinates in shape (B, N)
        '''
        # Need to make sure that this actually works
        batch_size = coordinates.shape[0]
        n_coordinates = coordinates.shape[1]  # number of points * 2
        reshaped_coordinates = coordinates.reshape(batch_size, -1, 2)
        ones = torch.ones((batch_size, n_coordinates // 2, 1),
                          dtype=torch.float32)
        reshaped_coordinates_with_ones = torch.cat(
            [reshaped_coordinates, ones],
            dim=-1)
        # does this actually works
        batch_transformation_matrix_transposed = torch.transpose(
            batch_tranformation_matrix, 1, 2)
        # batch matrix multiplication
        output_coordinates = reshaped_coordinates_with_ones @ \
            batch_transformation_matrix_transposed
        output_coordinates_reshaped = output_coordinates.reshape(batch_size,
                                                                 -1)
        return output_coordinates_reshaped

    def check_coordinates_validity(self, batched_cordinates: torch.Tensor,
                                   img_height: int, img_width: int):
        r'''This function checks the validity of coordinates with respect to
        image width and height

        Args:
            batched_coordinate (torch.Tensor): This is the transformed
                coordinates.
            img_height (int): The height of the image.
            img_width (int): The width of the image.
        Returns:
            torch.Tensor: A boolean mask indicates the validity of the
                coordinates
        '''
        batch_size = batched_cordinates.shape[0]
        reshaped_coordinates = batched_cordinates.reshape(batch_size, -1, 2)
        mask_reshaped = torch.empty(reshaped_coordinates.shape,
                                    dtype=torch.bool)
        # validating the y-coordinates
        mask_reshaped[:, :, 1] = ((0 <= reshaped_coordinates[:, :, 1]) <=
                                  img_height)
        # validating the x-coordinates
        mask_reshaped[:, :, 0] = ((0 <= reshaped_coordinates[:, :, 0]) <=
                                  img_width)
        # valid points mask (with both x and y coordinates are valid)
        valid_points = torch.logical_and(mask_reshaped[:, :, 1],
                                         mask_reshaped[:, :, 0])
        # setting the validation of the coordinates
        mask_reshaped[:, :, 1] = valid_points
        mask_reshaped[:, :, 0] = valid_points
        mask = mask_reshaped.reshape(batch_size, -1)
        return mask

    def __call__(self, tensors: Tuple[torch.tensor, torch.Tensor,
                                      torch.Tensor]):
        image_tensor, coordinates, mask = tensors
        r'''The actual call of Transformation of the batch images

        Args:
            image_tensor (torch.Tensor): The image batch to transform.
            coordinates (torch.Tensor): The Facial Keypoints coordinates.
            mask (torch.Tensor): The validity mask of the coordinates.
        Returns:
            transformed_images (torch.Tensor): The Transformed images.
            transformed_coordinates (torch.Tensor): The Transformed coordinates
            new_mask (torch.Tensor): The new coordinates validity mask
        '''
        batch_size, _, img_height, img_width = image_tensor.shape
        # Calculating the attributes to calculate the transformation matrix
        translations = self.generate_random_translations(batch_size,
                                                         img_height, img_width)
        centers = self.generate_image_center_for_batch(batch_size, img_height,
                                                       img_width)
        angles = self.generate_random_angles(batch_size)
        scales = self.generate_scale_factors(batch_size)
        shear_x = self.generate_shear_factors(batch_size,
                                              self.shear_factor_x_range)
        shear_y = self.generate_shear_factors(batch_size,
                                              self.shear_factor_y_range)
        # Calculating the Transformation Matrix
        batch_affine_matrix = get_affine_matrix2d(
            translations, centers, scales, angles, shear_x, shear_y)
        batch_affine_matrix = batch_affine_matrix[:, :2, :]
        transformed_images = warp_affine(
            image_tensor, batch_affine_matrix,
            dsize=(img_height, img_width))
        # Transform coordinates
        transformed_coordinates = self.transform_coordinates(
            batch_affine_matrix,
            coordinates)
        # Check Coordinate validatity
        transformed_coordinates_validity_mask = \
            self.check_coordinates_validity(
                transformed_coordinates,
                img_height, img_width)
        # Logical anging between the old and new mask
        new_mask = torch.logical_and(mask,
                                     transformed_coordinates_validity_mask)
        # Return output values
        return transformed_images, transformed_coordinates, new_mask


class RandomBatchHorizonalFlipping:
    r'''Randomly horizontally flip images from a batch of images
    '''
    def __init__(self, p: float = 0.5):
        r'''Args:
            p (float): the flipping probability
        '''
        self.p = p

    def __call__(self, tensors: Tuple[torch.tensor, torch.Tensor,
                                      torch.Tensor]):
        image_batch, coordinates, mask = tensors
        r'''Applies the random flipping to the batch and coordinates,
        It ignores the mask.

        Args:
            image_batch (torch.Tensor): The batch of images to flip.
            coordinates (torch.Tensor): The Facial Keypoints.
            mask (torch.Tensor): The Validity mask of the coordinates.
        Returns:
            transformed_images (torch.Tensor): The Flipped images.
            transformed_coordinate (torch.Tensor): The Transformed coordinates.
            mask (torch.Tensor): The orginal mask
        '''
        batch_size, _, img_height, img_width = image_batch.shape
        n_points = coordinates.shape[1] // 2
        random_values = torch.rand(batch_size)
        flip_images_tensor = (random_values < self.p)
        flipped_images = hflip(image_batch)
        flip_images_tensor_for_images = flip_images_tensor\
            .reshape(batch_size, 1, 1, 1)
        center_point_tensor = torch.tensor(
            [img_width / 2, 0], dtype=torch.float32)\
            .repeat(batch_size, n_points)  # [(X, Y), (X, Y), ...]
        flipped_coordinates = torch.abs(coordinates - 2 * center_point_tensor)
        flip_images_tensor_for_coordiates = flip_images_tensor\
            .reshape(batch_size, 1)
        transformed_images = flipped_images * flip_images_tensor_for_images + \
            image_batch * torch.logical_not(flip_images_tensor_for_images)
        transformed_coordinate = flipped_coordinates * \
            flip_images_tensor_for_coordiates + \
            coordinates * torch.logical_not(flip_images_tensor_for_coordiates)
        return transformed_images, transformed_coordinate, mask
